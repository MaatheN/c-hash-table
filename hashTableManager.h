//
//  hashTableManager.h
//  hash table
//
//  Created by Tatadmound on 23/04/2021.
//

#ifndef hashTableManager_h
#define hashTableManager_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StudentType.h"

//init table
Student* initStudentTable(void);
//hash intern function
int hashName(char* name);
//add a student
char addStudent(Student* studentTable,char* name, int age);
//char addStudent(Student* studentTable,char* name, int age);
//remove a student
char removeStudent(Student* studentTable, char* name);
//find a student by his name
Student findStudent(Student* studentTable, char* name);
char showStudents(Student* studentTable, Student student);
#endif /* hashTableManager_h */
