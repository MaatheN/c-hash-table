//
//  main.c
//  hash table
//
//  Created by Tatadmound on 23/04/2021.
//

#include <stdio.h>
#include "hashTableManager.h"

int main(int argc, const char * argv[]) {
    //create class list
    Student students[100];
    //add "Tom Franky" , 22 years old
    addStudent(students, "Tom Franky", 22);
    //and "Annissa Marvin", 24 years old
    addStudent(students, "Annissa Marvin", 24);
    //and "Jessy Kimberlaide", 19 years old
    addStudent(students, "Jessy Kimberlaide", 19);
    
    //find "Annissa Marvin" age
    Student annissa = findStudent(students, "Annissa Marvin");
    showStudents(students, annissa);
    
    removeStudent(students, "Annissa Marvin");
    showStudents(students, annissa);
    return 0;
}
