//
//  hashTableManager.c
//  hash table
//
//  Created by Tatadmound on 23/04/2021.
//

#include "hashTableManager.h"

/*Student* initStudentTable(){
    Student students[100];
    return students;
}*/

int hashName(char* name){
    int nameLength = strlen(name);
    int hashAdding = 0;
    for (int nameIndex=0; nameIndex<nameLength; nameIndex++) {
        printf("%d + %d(%c)", hashAdding, *(name+nameIndex), *(name+nameIndex));
        hashAdding+=*(name+nameIndex);
        printf(" = %d\n", hashAdding);
    }
    int hash = hashAdding%100;
    return hash;
}

char addStudent(Student* studentTable,char* name, int age){
    printf("%s\n", name);
    //create new Student
    Student newStudent;
    newStudent.age = age;
    newStudent.name = name;
    //hash name
    int hash = hashName(name);
    printf("STUDENT HASH : %d\n", hash);
    //assign it in the table
    studentTable[hash] = newStudent;
    return 0;
}

char removeStudent(Student* studentTable, char* name){
    printf("Deleting %s from student list\n", name);
    Student studentToDelete = findStudent(studentTable, name);
    studentToDelete.age = 0;
    studentToDelete.name = NULL;
    return 0;
}
Student findStudent(Student* studentTable, char* name){
    Student searchedStudent;
    int hash = hashName(name);
    searchedStudent = studentTable[hash];
    return searchedStudent;
}
char showStudents(Student* studentTable, Student student){
    if (student.age == 0 && student.name == NULL) {
        printf("This Student does not exist.\n");
    }else
        printf("%s's age : %d\n", student.name, student.age);
    return 0;
}
