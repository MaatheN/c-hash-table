//
//  StudentType.h
//  hash table
//
//  Created by Tatadmound on 23/04/2021.
//

#ifndef StudentType_h
#define StudentType_h
typedef struct Student Student;
struct Student{
    int age;
    char* name;
};
#endif /* StudentType_h */
